package com.example.praveen.shopping;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {
    RadioGroup rg;
    RadioButton rb_gender;
    CheckBox cb;
    Button bt;
    String en_user;
    String en_pass;
    String mobile;
    String mail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);
        rg = (RadioGroup) findViewById(R.id.radiogroup);
        cb = (CheckBox) findViewById(R.id.checkbox);
        bt = (Button) findViewById(R.id.registerbutton);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                                          @Override
                                          public void onCheckedChanged(RadioGroup group, int checkedId) {
                                              int selectedId = rg.getCheckedRadioButtonId();
                                              rb_gender = (RadioButton) findViewById(selectedId);
                                          }
                                      }
        );

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean res) {
                if (res) {
                    bt.setEnabled(true);
                } else {
                    bt.setEnabled(false);
                }
            }
        });
    }

    public void registerClick(View v) {

        EditText et_name = (EditText) findViewById(R.id.et_name);
        EditText et_pass = (EditText) findViewById(R.id.et_pass);
        EditText et_mob = (EditText) findViewById(R.id.et_mob);
        EditText et_mail = (EditText) findViewById(R.id.et_mail);

        en_user = et_name.getText().toString();
        en_pass = et_pass.getText().toString();
        mobile = et_mob.getText().toString();
        mail = et_mail.getText().toString();

        if (cb.isChecked()) {
            Toast.makeText(RegisterActivity.this, "Thank you for accepting terms and conditions", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(RegisterActivity.this, "Please accept to continue", Toast.LENGTH_SHORT).show();
        }


        if (en_user.isEmpty()){
            et_name.setError("username cannot be empty");
        }
        if (en_pass.isEmpty()){
            et_pass.setError("Paswword cannot be empty");
        }
        if (mobile.isEmpty()){
            et_mob.setError("Phone Number cannot be empty");
        }
        else if(mobile.trim().length() <10 || mobile.trim().length() > 10){
            et_mob.setError("Invalid Phone Number");
        }
        if (mail.isEmpty()){
            et_mail.setError("Mail cannot be empty");
        }

        if (en_pass.trim().length() > 0 && mail.trim().length() > 0 && mobile.trim().length() == 10 && en_user.trim().length() > 0 ){
            Intent i = new Intent(RegisterActivity.this, OTPActivity.class);
            i.putExtra("MOBILE", mobile);
            startActivity(i);
        }

    }


}

