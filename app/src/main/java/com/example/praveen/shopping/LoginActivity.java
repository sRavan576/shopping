package com.example.praveen.shopping;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    EditText uname;
    EditText pass;
    Button bt;
    LinearLayout lt;
    private View view;
    String en_user;
    String en_pass;


    @Override
    protected void onCreate(Bundle a) {
        super.onCreate(a);
        SharedPreferences sp= getSharedPreferences("MYUSERDATA",MODE_PRIVATE);
        String en_user=sp.getString("NAME","");
        String en_pass=sp.getString("PASSWORD","");
        if (en_user.equals("Sravan") && en_pass.equals("576")) {
            Intent i = new Intent(LoginActivity.this, BrandlistActivity.class);
            startActivity(i);
        } else {
            setContentView(R.layout.activity_login);
        }
        bt = (Button) findViewById(R.id.button);
        lt = (LinearLayout) findViewById(R.id.linearlayout);
        //Intent i = new Intent();
        // Name = i.getStringExtra("NAME");

    }

    public void clickContinue(View V) {

        uname = (EditText) findViewById(R.id.username);
        pass = (EditText) findViewById(R.id.password);
        String en_user = uname.getText().toString();
        String en_pass = pass.getText().toString();

        if (en_user.isEmpty() && en_pass.isEmpty()) {
            uname.setError("username cannot be empty");
            pass.setError("password cannot be empty");
        } else if (en_user.isEmpty()) {
            uname.setError("username cannot be empty");
        } else if (en_pass.isEmpty()) {
            pass.setError("password cannot be empty");
        }
        if (en_user.equals("Sravan") && en_pass.equals("576")) {

            SharedPreferences sp=getSharedPreferences("MYUSERDATA",MODE_PRIVATE);
            SharedPreferences.Editor editor=sp.edit();
            editor.putString("NAME",en_user);
            editor.putString("PASSWORD",en_pass);
            editor.commit();

            Intent i = new Intent(LoginActivity.this, BrandlistActivity.class);
            startActivity(i);
        }
        else {
            if (!en_user.equals("Sravan")) {
                Toast.makeText(LoginActivity.this, "Username is Incorrect", Toast.LENGTH_SHORT).show();
            } else if (!en_pass.equals("576")) {
                Toast.makeText(this, "Incorrect password", Toast.LENGTH_SHORT).show();
            } else if (!en_user.equals("Sravan") && !en_pass.equals("576")) {
                Toast.makeText(this, "Username and Password invalid", Toast.LENGTH_SHORT).show();
            }

        }

    }
    public void gotoRegister(View view){

        Intent i=new Intent(LoginActivity.this,RegisterActivity.class);
        startActivity(i);
    }
}



