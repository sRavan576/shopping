package com.example.praveen.shopping;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                LatLng ameerpet = new LatLng(17.4373927, 78.4442951);
                googleMap.addMarker(new MarkerOptions().position(ameerpet));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(ameerpet));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(14));


                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        marker.showInfoWindow();
                        return false;
                    }
                });
                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {
                        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                        View v = layoutInflater.inflate(R.layout.infobox, null);
                        TextView tv_lat = (TextView) v.findViewById(R.id.tv_lat);
                        TextView tv_long = (TextView) v.findViewById(R.id.tv_longi);
                        TextView tv_details = (TextView) v.findViewById(R.id.tv_details);
                        LatLng latLng = marker.getPosition();

                        tv_lat.setText("Latitude:" + latLng.latitude);
                        tv_long.setText("Longitude:" + latLng.longitude);

                        Geocoder geocoder = new Geocoder(MapsActivity.this);
                        try {
                            List<Address> list = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                            Log.e("PLACE_DETAILS", list.get(0) + "");

                            tv_details.setText("" + list.get(0).getAddressLine(0));


                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        return v;
                    }
                });
            }
        });


    }

    public void payClick(View view){
      Button paybutton=(Button)findViewById(R.id.paybutton);
        Intent intent=new Intent(MapsActivity.this,PaymentActivity.class);
        startActivity(intent);
    }
}