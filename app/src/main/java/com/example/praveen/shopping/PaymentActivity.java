package com.example.praveen.shopping;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class PaymentActivity extends AppCompatActivity {
    CheckBox c_box1;
    CheckBox c_box2;
    CheckBox c_box3;
    CheckBox c_box4;
    Button cb_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        c_box1 = (CheckBox)findViewById(R.id.c_box1);
        c_box2 = (CheckBox)findViewById(R.id.c_box2);
        c_box3 = (CheckBox)findViewById(R.id.c_box3);
        c_box4 = (CheckBox)findViewById(R.id.c_box4);

        c_box1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    Snackbar.make(compoundButton, "Payment via paytm", Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                }
            }
        });

        c_box2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    Snackbar.make(compoundButton, "Payment via NetBanking", Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                }
            }
        });

        c_box3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    Snackbar.make(compoundButton, "Payment via Credit/Debit card", Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                }
            }
        });
        c_box4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    Snackbar.make(compoundButton, "cash on Delivery", Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                }
            }
        });
    }
    public void proceedClick(View view){

        Intent intent=new Intent(PaymentActivity.this,ConformationActivity.class);
        startActivity(intent);

    }
}
