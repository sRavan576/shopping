package com.example.praveen.shopping;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class OTPActivity extends AppCompatActivity {

    int random_num;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        Intent i=getIntent();
        String mob=i.getStringExtra("MOBILE");
        TextView tv=(TextView)findViewById(R.id.tv_text);
        tv.setText("OTP IS SEND ON "+mob);

        Random random=new Random();
        int min=1111;
        int max=9999;
        random_num=random.nextInt((max-min)+1);
        Log.e("RANDOM_NO",random_num+"");
        String msg="THE OTP FOR OTPAPP IS" +random_num;
        try {
            SmsManager sm = SmsManager.getDefault();
            sm.sendTextMessage(mob, null, msg, null, null);
            Toast.makeText(this, "OTP SENT", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e){
            Toast.makeText(this, "SOMETHING WENT WRONG", Toast.LENGTH_SHORT).show();
        }

    }

    public void verifyOtp(View v){
        EditText et=(EditText)findViewById(R.id.et_otp);
        String et_no=et.getText().toString();
        int enteredOTP=Integer.parseInt(et_no);

        if (enteredOTP==random_num){
            Intent i=new Intent(OTPActivity.this,LoginActivity.class);
            startActivity(i);
        }
        else {
            Toast.makeText(this, "INVALID OTP", Toast.LENGTH_SHORT).show();
            et.setText("");
        }

    }
}


